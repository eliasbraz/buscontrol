package br.com.buscontrol.util;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

public class ImageUtil {

	public static boolean verifyImageResolution(String filepath) {
		try {
			File file = new File(filepath);

			BufferedImage bi;
			bi = ImageIO.read(file);
			int height = bi.getHeight();
			int width = bi.getWidth();

			if (width > 920 || height > 920) {
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	public static String convertImageAndDecrease(String filepath) {
		try {
			File file = new File(filepath);

			BufferedImage bi;
			bi = ImageIO.read(file);
			int height = bi.getHeight();
			int width = bi.getWidth();

			if (width > 920 || height > 920) {
				ImageInputStream iis = ImageIO.createImageInputStream(file);
				Iterator<ImageReader> imageReaders = ImageIO.getImageReaders(iis);

				String formatImage = null;

				while (imageReaders.hasNext()) {
					ImageReader reader = imageReaders.next();
					formatImage = reader.getFormatName().toLowerCase();
					System.out.println("Formato da foto: " + formatImage + "\n");
				}

				System.out.println("Convertendo a imagem para png e diminuindo o tamanho\n");

				File newFile = new File(file.getPath().replace(formatImage, "png"));

				Integer newHeight = (int) (height - (0.50 * height));
				Integer newWidth = (int) (width - (0.50 * width));

				System.out.println("Nova altura: " + newHeight);
				System.out.println("Nova largura: " + newWidth + "\n");

				BufferedImage newbi = new BufferedImage(newWidth, newHeight, bi.getType());
				Graphics2D g2d = newbi.createGraphics();
				g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
				g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
				g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
				g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
				g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
				g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
				g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
				g2d.drawImage(bi, 0, 0, newWidth, newHeight, 0, 0, width, height, null);
				g2d.dispose();

				ImageIO.write(newbi, formatImage, newFile);

				return newFile.toString();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return filepath;
	}

	public static void decreaseImage(String filepath) {
		try {

			File file = new File(filepath);

			BufferedImage bi;
			bi = ImageIO.read(file);
			int height = bi.getHeight();
			int width = bi.getWidth();

			Integer newHeight = 200;
			Integer newWidth = (int) ((200 * width) / height);

			BufferedImage newbi = new BufferedImage(newWidth, newHeight, bi.getType());
			Graphics2D g2d = newbi.createGraphics();
			g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g2d.drawImage(bi, 0, 0, newWidth, newHeight, 0, 0, width, height, null);
			g2d.dispose();

			ImageIO.write(newbi, "png", file);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}