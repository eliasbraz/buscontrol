package br.com.buscontrol.util;

import javax.faces.context.FacesContext;

public class FacesUtil {

	public static String getLabel(String label) {
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getApplication().getResourceBundle(context, "label").getString(label);
	}
}
