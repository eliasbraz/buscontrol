package br.com.buscontrol.service;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResidentServiceModel {

	private Integer id;
	private String name;
	private String document;
	private String apartment;
	private String building;
	private String condominium;

	public ResidentServiceModel() {
	}

	public ResidentServiceModel(Integer id, String name, String document, String apartment, String building,
			String condominium) {
		super();
		this.id = id;
		this.name = name;
		this.document = document;
		this.apartment = apartment;
		this.building = building;
		this.condominium = condominium;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getApartment() {
		return apartment;
	}

	public void setApartment(String apartment) {
		this.apartment = apartment;
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getCondominium() {
		return condominium;
	}

	public void setCondominium(String condominium) {
		this.condominium = condominium;
	}

}
