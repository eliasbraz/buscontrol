package br.com.buscontrol.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.buscontrol.dao.ResidentDAO;
import br.com.buscontrol.model.Resident;

@SuppressWarnings("deprecation")
@ManagedBean
@RequestScoped
@Path("/resident")
public class ResidentService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private ResidentDAO dao;

	/*
	 * @GET
	 * 
	 * @Path("{id}")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON) public ResidentServiceModel
	 * getResident(@PathParam("id") int id) { Resident resident =
	 * dao.findbyid(id);
	 * 
	 * if (resident != null) { ResidentServiceModel model = new
	 * ResidentServiceModel(resident.getId(), resident.getName(),
	 * resident.getDocument(), resident.getApartment().getNumber(),
	 * resident.getApartment().getBuilding().getName(),
	 * resident.getApartment().getBuilding().getCondominium().getName());
	 * 
	 * return model; }
	 * 
	 * return new ResidentServiceModel(); }
	 */

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<ResidentServiceModel> getResidents() {
		List<Resident> residents = dao.listavaiables();
		List<ResidentServiceModel> residentServiceModels = new ArrayList<>();

		if (!residents.isEmpty()) {
			for (Resident resident : residents) {
				ResidentServiceModel model = new ResidentServiceModel(resident.getId(), resident.getName(),
						resident.getDocument(), resident.getApartment().getNumber(),
						resident.getApartment().getBuilding().getName(),
						resident.getApartment().getBuilding().getCondominium().getName());

				residentServiceModels.add(model);
			}

			return residentServiceModels;
		}

		residentServiceModels.add(new ResidentServiceModel());

		return residentServiceModels;
	}

}
