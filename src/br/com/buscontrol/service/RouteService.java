package br.com.buscontrol.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.buscontrol.dao.RouteDAO;
import br.com.buscontrol.model.Route;

@RequestScoped
@Path("/routes")
public class RouteService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private RouteDAO dao;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Route> allroutes() {
		ArrayList<Route> routes = new ArrayList<>();
		routes = dao.listavaiables();
		
		for(Route route : routes) {
			route.setUser(null);
		}

		return routes;
	}
}
