package br.com.buscontrol.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.buscontrol.dao.TripDAO;
import br.com.buscontrol.model.Trip;

@RequestScoped
@Path("/trip")
public class TripService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private TripDAO dao;

	/*
	 * @POST
	 * 
	 * @Produces(MediaType.APPLICATION_JSON) public boolean tripSave(Trip trip)
	 * { if (dao.saveorupdate(trip) != null) { return true; }
	 * 
	 * return false; }
	 */

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Trip> tripsSave(List<Trip> trips) {
		List<Trip> tripsWithError = new ArrayList<>();

		if (!trips.isEmpty()) {
			for (Trip trip : trips) {
				dao.saveorupdate(trip);
			}
		}

		return tripsWithError;
	}

}
