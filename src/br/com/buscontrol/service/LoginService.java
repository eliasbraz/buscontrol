package br.com.buscontrol.service;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.buscontrol.control.LoginControl;
import br.com.buscontrol.model.User;

@RequestScoped
@Path("/login")
public class LoginService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private LoginControl logincontrol;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public User login(User user) {
		user = logincontrol.isValidLogin(user.getUsername(), user.getPassword());

		if (user != null) {
			// user.setPassword(null);
			return user;
		}
		return new User();
	}
}
