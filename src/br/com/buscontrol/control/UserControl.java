package br.com.buscontrol.control;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import br.com.buscontrol.model.User;
import br.com.buscontrol.model.UserType;

@Named(value = "usercontrol")
@SessionScoped
public class UserControl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static final String INJECTION_NAME = "#{usuariocontrol}";

	@Inject
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void authentication() {

	}

	public boolean isAdmin() {
		return UserType.ADMINISTRATOR.equals(user.getType());
	}

	public boolean isOperator() {
		return UserType.OPERATOR.equals(user.getType());
	}

	public boolean isDriver() {
		return UserType.DRIVER.equals(user.getType());
	}
	
	public boolean isLogged() {
		return user != null && user.getId() != null;
	}

	public String logOut() {
		((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).invalidate();

		return "/login.xhtml?faces-redirect=true";
	}
}
