package br.com.buscontrol.control;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.omnifaces.util.Messages;

import br.com.buscontrol.dao.UserDAO;
import br.com.buscontrol.model.User;

@SuppressWarnings("deprecation")
@Named(value = "logincontrol")
@RequestScoped
public class LoginControl implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	@ManagedProperty(value = UserControl.INJECTION_NAME)
	private UserControl usercontrol;

	private String username;
	private String password;

	@Inject
	private User user;

	@Inject
	private UserDAO dao;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User isValidLogin(String username, String password) {
		user = dao.findbyusername(username);

		if (user == null || !password.equals(user.getPassword())) {
			return null;
		}

		return user;
	}

	public String sign() {
		user = isValidLogin(username, password);

		if (user != null) {
			usercontrol.setUser(user);

			((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getSession()
					.setAttribute(username, user);

			return "/templates/template.xhtml?faces-redirect=true";
		}

		Messages.addGlobalError("Username or password invalid!");
		return null;
	}
}
