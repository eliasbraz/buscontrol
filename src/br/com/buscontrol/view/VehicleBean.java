package br.com.buscontrol.view;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;
import org.primefaces.event.SelectEvent;

import br.com.buscontrol.control.UserControl;
import br.com.buscontrol.dao.VehicleDAO;
import br.com.buscontrol.dao.VehicleTypeDAO;
import br.com.buscontrol.model.Vehicle;
import br.com.buscontrol.model.VehicleType;
import br.com.buscontrol.util.FacesUtil;

@Named(value = "vbean")
@ViewScoped
public class VehicleBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private VehicleDAO daovehicle;

	@Inject
	private VehicleTypeDAO daovehicletype;

	@Inject
	private ConfPreferences preferences;

	@Inject
	private UserControl usercontrol;

	private Vehicle vehicle;
	private Vehicle selectedvehicle;
	private ArrayList<Vehicle> vehicles = null;
	private ArrayList<VehicleType> vehicletypes = null;

	@PostConstruct
	public void init() {
		vehicle = new Vehicle();
		vehicles = daovehicle.findall();
		vehicletypes = daovehicletype.findall();
		preferences.setDisabled(false);
		preferences.setWindowtype("new");
		preferences.setTitle("Vehicles");
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public ArrayList<Vehicle> getVehicles() {
		return vehicles;
	}

	public Vehicle getSelectedvehicle() {
		return selectedvehicle;
	}

	public void setSelectedvehicle(Vehicle selectedvehicle) {
		this.selectedvehicle = selectedvehicle;
	}

	public void setVehicles(ArrayList<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	public ArrayList<VehicleType> getVehicletypes() {
		return vehicletypes;
	}

	public void setVehicletypes(ArrayList<VehicleType> vehicletypes) {
		this.vehicletypes = vehicletypes;
	}

	public void listall() {
		try {
			vehicles = daovehicle.findall();
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorlisting"));
		}
	}

	public void saveorupdate() {
		try {
			vehicle.setUser(usercontrol.getUser());
			daovehicle.saveorupdate(vehicle);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("savesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorsaving"));
		}
	}

	public void preparebean(SelectEvent event) {
		setVehicle(getSelectedvehicle());
		preferences.setDisabled(true);
		preferences.setWindowtype("open");
	}

	public void disable(ActionEvent event) {
		try {
			vehicle = (Vehicle) event.getComponent().getAttributes().get("selectedvehicle");

			daovehicle.delete(vehicle);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("disablesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errordisable"));
		}
	}
}
