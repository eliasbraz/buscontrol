package br.com.buscontrol.view;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;
import org.primefaces.event.SelectEvent;

import br.com.buscontrol.control.UserControl;
import br.com.buscontrol.dao.CompanyDAO;
import br.com.buscontrol.model.Company;
import br.com.buscontrol.util.FacesUtil;

@Named(value = "cbean")
@ViewScoped
public class CompanyBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private CompanyDAO daocompany;

	@Inject
	private ConfPreferences preferences;

	@Inject
	private UserControl usercontrol;

	private Company company;
	private Company seleteccompany;
	private ArrayList<Company> companies;

	@PostConstruct
	public void init() {
		company = new Company();
		companies = daocompany.findall();
		preferences.setDisabled(false);
		preferences.setWindowtype("new");
		preferences.setTitle("Companies");
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Company getSeleteccompany() {
		return seleteccompany;
	}

	public void setSeleteccompany(Company seleteccompany) {
		this.seleteccompany = seleteccompany;
	}

	public ArrayList<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(ArrayList<Company> companies) {
		this.companies = companies;
	}

	public void listall() {
		try {
			companies = daocompany.findall();
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorlisting"));
		}
	}

	public void saveorupdate() {
		try {
			company.setUser(usercontrol.getUser());
			daocompany.saveorupdate(company);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("savesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorsaving"));
		}
	}

	public void preparebean(SelectEvent event) {
		setCompany(getSeleteccompany());
		preferences.setDisabled(true);
		preferences.setWindowtype("open");
	}

	public void disable(ActionEvent event) {
		try {
			company = (Company) event.getComponent().getAttributes().get("selectedcompany");

			daocompany.delete(company);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("disablesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errordisable"));
		}
	}
}
