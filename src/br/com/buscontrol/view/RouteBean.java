package br.com.buscontrol.view;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;
import org.primefaces.event.SelectEvent;

import br.com.buscontrol.control.UserControl;
import br.com.buscontrol.dao.RouteDAO;
import br.com.buscontrol.model.Route;
import br.com.buscontrol.util.FacesUtil;

@Named(value = "rbean")
@ViewScoped
public class RouteBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private RouteDAO daoroute;

	@Inject
	private ConfPreferences preferences;

	@Inject
	private UserControl usercontrol;

	private Route route;
	private Route selectedroute;
	private ArrayList<Route> routes = null;

	@PostConstruct
	public void init() {
		route = new Route();
		routes = daoroute.findall();
		preferences.setDisabled(false);
		preferences.setWindowtype("new");
		preferences.setTitle("Routes");
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public Route getSelectedroute() {
		return selectedroute;
	}

	public void setSelectedroute(Route selectedroute) {
		this.selectedroute = selectedroute;
	}

	public ArrayList<Route> getRoutes() {
		return routes;
	}

	public void setRoutes(ArrayList<Route> routes) {
		this.routes = routes;
	}

	public void listall() {
		try {
			routes = daoroute.findall();
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorlisting"));
		}
	}

	public void saveorupdate() {
		try {
			route.setUser(usercontrol.getUser());
			daoroute.saveorupdate(route);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("savesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorsaving"));
		}
	}

	public void preparebean(SelectEvent event) {
		setRoute(getSelectedroute());
		preferences.setDisabled(true);
		preferences.setWindowtype("open");
	}

	public void disable(ActionEvent event) {
		try {
			route = (Route) event.getComponent().getAttributes().get("selectedroute");

			daoroute.delete(route);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("disablesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errordisable"));
		}
	}
}
