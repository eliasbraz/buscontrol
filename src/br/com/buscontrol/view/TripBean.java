package br.com.buscontrol.view;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import br.com.buscontrol.dao.ResidentDAO;
import br.com.buscontrol.dao.ResidentTripDAO;
import br.com.buscontrol.dao.RouteDAO;
import br.com.buscontrol.dao.UserDAO;
import br.com.buscontrol.model.Resident;
import br.com.buscontrol.model.ResidentTrip;
import br.com.buscontrol.model.Route;
import br.com.buscontrol.model.User;

@ViewScoped
@Named(value = "tbean")
public class TripBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private ResidentTripDAO daotrip;

	@Inject
	private RouteDAO daoroute;

	@Inject
	private UserDAO daouser;

	@Inject
	private ResidentDAO daoresident;

	private ResidentTrip residenttrip;
	private ArrayList<ResidentTrip> residentstrip;
	private ArrayList<Route> routes;
	private ArrayList<User> drivers;
	private ArrayList<Resident> residents;

	@PostConstruct
	public void init() {
		residenttrip = new ResidentTrip();
		residentstrip = daotrip.findall();
		routes = daoroute.findall();
		drivers = daouser.listdrivers();
		residents = daoresident.findall();
	}

	public void tripclean() {
		residenttrip = new ResidentTrip();
		residentstrip = daotrip.findall();
	}

	public void tripfilter() {
		try {
			residentstrip = daotrip.residenttripfilterred(residenttrip);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	// ----------------------------------------------------------------------------------------------------------------------------------------------------------

	public ResidentTrip getResidenttrip() {
		return residenttrip;
	}

	public void setResidenttrip(ResidentTrip residenttrip) {
		this.residenttrip = residenttrip;
	}

	public ArrayList<ResidentTrip> getResidentstrip() {
		return residentstrip;
	}

	public void setResidentstrip(ArrayList<ResidentTrip> residentstrip) {
		this.residentstrip = residentstrip;
	}

	public ArrayList<Route> getRoutes() {
		return routes;
	}

	public void setRoutes(ArrayList<Route> routes) {
		this.routes = routes;
	}

	public ArrayList<User> getDrivers() {
		return drivers;
	}

	public void setDrivers(ArrayList<User> drivers) {
		this.drivers = drivers;
	}

	public ArrayList<Resident> getResidents() {
		return residents;
	}

	public void setResidents(ArrayList<Resident> residents) {
		this.residents = residents;
	}

}
