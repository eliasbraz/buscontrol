package br.com.buscontrol.view;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;
import org.primefaces.event.SelectEvent;

import br.com.buscontrol.control.UserControl;
import br.com.buscontrol.dao.VehicleTypeDAO;
import br.com.buscontrol.model.VehicleType;
import br.com.buscontrol.util.FacesUtil;

@Named(value = "vtbean")
@ViewScoped
public class VehicleTypeBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private VehicleTypeDAO dao;

	@Inject
	private ConfPreferences preferences;

	@Inject
	private UserControl usercontrol;

	private VehicleType vehiclet;
	private VehicleType selectedvehiclet;
	private ArrayList<VehicleType> vehiclets = null;

	@PostConstruct
	public void init() {
		vehiclet = new VehicleType();
		vehiclets = dao.findall();
		preferences.setDisabled(false);
		preferences.setWindowtype("new");
		preferences.setTitle("Vehicle Types");
	}

	public VehicleType getVehiclet() {
		return vehiclet;
	}

	public void setVehiclet(VehicleType vehiclet) {
		this.vehiclet = vehiclet;
	}

	public VehicleType getSelectedvehiclet() {
		return selectedvehiclet;
	}

	public void setSelectedvehiclet(VehicleType selectedvehiclet) {
		this.selectedvehiclet = selectedvehiclet;
	}

	public ArrayList<VehicleType> getVehiclets() {
		return vehiclets;
	}

	public void setVehiclets(ArrayList<VehicleType> vehiclets) {
		this.vehiclets = vehiclets;
	}

	public void listall() {
		try {
			vehiclets = dao.findall();
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorlisting"));
		}
	}

	public void saveorupdate() {
		try {
			vehiclet.setUser(usercontrol.getUser());
			dao.saveorupdate(vehiclet);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("savesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorsaving"));
		}
	}

	public void preparebean(SelectEvent event) {
		setVehiclet(getSelectedvehiclet());
		preferences.setDisabled(true);
		preferences.setWindowtype("open");
	}

	public void disable(ActionEvent event) {
		try {
			vehiclet = (VehicleType) event.getComponent().getAttributes().get("selectedvehiclet");

			dao.delete(vehiclet);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("disablesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errordisable"));
		}
	}
}
