package br.com.buscontrol.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;
import org.primefaces.event.SelectEvent;

import br.com.buscontrol.dao.CompanyDAO;
import br.com.buscontrol.dao.UserDAO;
import br.com.buscontrol.model.Company;
import br.com.buscontrol.model.User;
import br.com.buscontrol.model.UserType;
import br.com.buscontrol.util.FacesUtil;

@Named(value = "userbean")
@ViewScoped
public class UserBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private UserDAO daouser;

	@Inject
	private ConfPreferences preferences;

	@Inject
	private CompanyDAO daocompany;

	private User User;
	private User selecteduser;
	private ArrayList<User> users;
	private ArrayList<UserType> usertypes;
	private ArrayList<Company> companies;

	@PostConstruct
	public void init() {
		User = new User();
		users = daouser.findall();
		preferences.setDisabled(false);
		preferences.setWindowtype("new");
		preferences.setTitle("Users");
		usertypes = new ArrayList<>(Arrays.asList(UserType.values()));
		companies = daocompany.listavaiables();
	}

	public User getUser() {
		return User;
	}

	public void setUser(User User) {
		this.User = User;
	}

	public User getSelecteduser() {
		return selecteduser;
	}

	public void setSelecteduser(User selecteduser) {
		this.selecteduser = selecteduser;
	}

	public ArrayList<User> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}

	public ArrayList<UserType> getUsertypes() {
		return usertypes;
	}

	public void setUsertypes(ArrayList<UserType> usertypes) {
		this.usertypes = usertypes;
	}

	public ArrayList<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(ArrayList<Company> companies) {
		this.companies = companies;
	}

	public void listall() {
		try {
			users = daouser.findall();
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorlisting"));
		}
	}

	public void saveorupdate() {
		try {
			daouser.saveorupdate(User);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("savesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorsaving"));
		}
	}

	public void preparebean(SelectEvent event) {
		setUser(getSelecteduser());
		preferences.setDisabled(true);
		preferences.setWindowtype("open");
	}

	public void disable(ActionEvent event) {
		try {
			User = (User) event.getComponent().getAttributes().get("selectedUser");

			daouser.delete(User);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("disablesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errordisable"));
		}
	}
}
