package br.com.buscontrol.view;

import java.io.Serializable;

import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

@Named(value = "preferences")
@ViewScoped
public class ConfPreferences implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String theme = "green";
	private boolean disabled = true;
	private boolean overlayMenu = false;
	private String windowtype = "open";
	private String title = "";

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public boolean isOverlayMenu() {
		return this.overlayMenu;
	}

	public void setOverlayMenu(boolean value) {
		this.overlayMenu = value;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public String getWindowtype() {
		return windowtype;
	}

	public void setWindowtype(String windowtype) {
		this.windowtype = windowtype;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
