package br.com.buscontrol.view;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;

import br.com.buscontrol.util.FacesUtil;
import br.com.buscontrol.control.UserControl;
import br.com.buscontrol.dao.UserDAO;
import br.com.buscontrol.model.User;

@Named(value = "tbbean")
@ViewScoped
public class TopbarBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String passwordcurrent = "";
	private String newpassword = "";
	private String confirmpassword = "";

	@Inject
	private UserDAO daouser;

	@Inject
	private UserControl usercontrol;

	@Inject
	private User user;

	public String getPasswordcurrent() {
		return passwordcurrent;
	}

	public void setPasswordcurrent(String passwordcurrent) {
		this.passwordcurrent = passwordcurrent;
	}

	public String getNewpassword() {
		return newpassword;
	}

	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}

	public String getConfirmpassword() {
		return confirmpassword;
	}

	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}

	public void save() {
		if (!newpassword.equals(confirmpassword)) {
			Messages.addGlobalError(FacesUtil.getLabel("newpassowrddifferentconfirmpassoword"));
		} else if (confirmpassword.equals(passwordcurrent)) {
			Messages.addGlobalError(FacesUtil.getLabel("confirmpassworddifferentpasswordcurrent"));
		} else {
			try {
				if (usercontrol.getUser().getPassword().equals(passwordcurrent)) {
					user = usercontrol.getUser();
					user.setPassword(confirmpassword);

					daouser.saveorupdate(user);

					Messages.addGlobalInfo(FacesUtil.getLabel("newpasswordsave"));
				} else {
					Messages.addGlobalError(FacesUtil.getLabel("incorrectpassword"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				Messages.addGlobalError(FacesUtil.getLabel("errorsaving"));
			}
		}
	}
}
