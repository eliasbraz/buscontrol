package br.com.buscontrol.view;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;
import org.primefaces.event.SelectEvent;

import br.com.buscontrol.control.UserControl;
import br.com.buscontrol.dao.BuildingDAO;
import br.com.buscontrol.dao.CondominiumDAO;
import br.com.buscontrol.model.Building;
import br.com.buscontrol.model.Condominium;
import br.com.buscontrol.util.FacesUtil;

@Named(value = "bbean")
@ViewScoped
public class BuildingBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private BuildingDAO daobuilding;

	@Inject
	private CondominiumDAO daocondominium;

	@Inject
	private ConfPreferences preferences;

	@Inject
	private UserControl usercontrol;

	private Building building;
	private Building selectedbuilding;
	private ArrayList<Building> buildings = null;
	private ArrayList<Condominium> condominiums = null;

	@PostConstruct
	public void init() {
		building = new Building();
		buildings = daobuilding.findall();
		condominiums = daocondominium.listavaiables();
		preferences.setDisabled(false);
		preferences.setWindowtype("new");
		preferences.setTitle("Buildings");
	}

	public Building getBuilding() {
		return building;
	}

	public void setBuilding(Building building) {
		this.building = building;
	}

	public Building getSelectedbuilding() {
		return selectedbuilding;
	}

	public void setSelectedbuilding(Building selectedbuilding) {
		this.selectedbuilding = selectedbuilding;
	}

	public ArrayList<Building> getBuildings() {
		return buildings;
	}

	public void setBuildings(ArrayList<Building> buildings) {
		this.buildings = buildings;
	}

	public ArrayList<Condominium> getCondominiums() {
		return condominiums;
	}

	public void setCondominiums(ArrayList<Condominium> condominiums) {
		this.condominiums = condominiums;
	}

	public void listall() {
		try {
			buildings = daobuilding.findall();
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorlisting"));
		}
	}

	public void saveorupdate() {
		try {
			building.setUser(usercontrol.getUser());
			daobuilding.saveorupdate(building);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("savesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorsaving"));
		}
	}

	public void preparebean(SelectEvent event) {
		setBuilding(getSelectedbuilding());
		preferences.setDisabled(true);
		preferences.setWindowtype("open");
	}

	public void disable(ActionEvent event) {
		try {
			building = (Building) event.getComponent().getAttributes().get("selectedbuilding");

			daobuilding.delete(building);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("disablesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errordisable"));
		}
	}
}
