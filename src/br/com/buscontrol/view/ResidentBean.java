package br.com.buscontrol.view;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.imageio.stream.FileImageOutputStream;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;
import org.primefaces.event.CaptureEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.CroppedImage;

import br.com.buscontrol.control.UserControl;
import br.com.buscontrol.dao.ApartmentDAO;
import br.com.buscontrol.dao.BuildingDAO;
import br.com.buscontrol.dao.CondominiumDAO;
import br.com.buscontrol.dao.ResidentDAO;
import br.com.buscontrol.model.Apartment;
import br.com.buscontrol.model.Building;
import br.com.buscontrol.model.Condominium;
import br.com.buscontrol.model.Resident;
import br.com.buscontrol.util.FacesUtil;
import br.com.buscontrol.util.ImageUtil;

@Named(value = "rsbean")
@ViewScoped
public class ResidentBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ResidentDAO daoresident;

	@Inject
	private ApartmentDAO daoapartment;

	@Inject
	private BuildingDAO daobuilding;

	@Inject
	private CondominiumDAO daocondominium;

	@Inject
	private ConfPreferences preferences;

	@Inject
	private UserControl userControl;

	private Resident resident;
	private Resident selectedresident;
	private ArrayList<Resident> residents = null;
	private ArrayList<Apartment> apartments = null;
	private ArrayList<Building> buildings = null;
	private ArrayList<Condominium> condominiums = null;
	private String filename;
	byte[] data;
	private CroppedImage croppedImage;

	@PostConstruct
	public void init() {
		resident = new Resident();
		residents = daoresident.findall();
		apartments = daoapartment.listavaiables();
		buildings = daobuilding.listavaiables();
		condominiums = daocondominium.listavaiables();
		preferences.setDisabled(false);
		preferences.setWindowtype("new");
		preferences.setTitle("Residents");
		filename = "";
	}

	public Resident getResident() {
		return resident;
	}

	public void setResident(Resident resident) {
		this.resident = resident;
	}

	public Resident getSelectedresident() {
		return selectedresident;
	}

	public void setSelectedresident(Resident selectedresident) {
		this.selectedresident = selectedresident;
	}

	public ArrayList<Resident> getResidents() {
		return residents;
	}

	public void setResidents(ArrayList<Resident> residents) {
		this.residents = residents;
	}

	public ArrayList<Apartment> getApartments() {
		return apartments;
	}

	public void setApartments(ArrayList<Apartment> apartments) {
		this.apartments = apartments;
	}
	
	public ArrayList<Building> getBuildings() {
		return buildings;
	}
	
	public void setBuildings(ArrayList<Building> buildings) {
		this.buildings = buildings;
	}
	
	public ArrayList<Condominium> getCondominiums() {
		return condominiums;
	}
	
	public void setCondominiums(ArrayList<Condominium> condominiums) {
		this.condominiums = condominiums;
	}

	public String getFilename() {
		return filename;
	}

	public String getRandomImageName() {
		return String.valueOf((int) (Math.random() * 100000));
	}

	public CroppedImage getCroppedImage() {
		return croppedImage;
	}

	public void setCroppedImage(CroppedImage croppedImage) {
		this.croppedImage = croppedImage;
	}

	public void listall() {
		try {
			residents = daoresident.findall();
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorlisting"));
		}
	}

	public void saveorupdate() {
		try {
			resident.setUser(userControl.getUser());
			resident = daoresident.saveorupdate(resident);

			if (data != null) {
//				cropperimagesave(filepath().replace(filename, resident.getId().toString()));
				imagesave(filepath().replace(filename, resident.getId().toString()));
			}

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("savesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorsaving"));
		}
	}

	public void preparebean(SelectEvent event) {
		setResident(getSelectedresident());
		//filename = Integer.toString(getSelectedresident().getId()).concat("_cropper");
		filename = Integer.toString(getSelectedresident().getId());
		preferences.setDisabled(true);
		preferences.setWindowtype("open");
	}

	public void disable(ActionEvent event) {
		try {
			resident = (Resident) event.getComponent().getAttributes().get("selectedresident");

			daoresident.delete(resident);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("disablesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errordisable"));
		}
	}

	public void oncapture(CaptureEvent event) {
		data = event.getData();

//		filename = getRandomImageName();
		filename = UUID.randomUUID().toString();

		imagesave(filepath());
//		imagesave("C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\images\\" + filename + ".png");
	}

//	private String filepath() {
//		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//		return ec.getRealPath("") + File.separator + "resources" + File.separator + filename + ".png";
//	}

	private String filepath() {
		return "/opt/tomcat/latest/webapps/images/" + filename + ".png";
	}

	private void imagesave(String filename) {
		FileImageOutputStream fios;
		File file;

		try {
			file = new File(filename);
			fios = new FileImageOutputStream(file);
			fios.write(data, 0, data.length);
			fios.close();
		} catch (IOException e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorcam"));
		}
	}

	private void cropperimagesave(String filename) {
		FileImageOutputStream fios;
		File file;

		try {
			file = new File(filename);
			fios = new FileImageOutputStream(file);
			fios.write(croppedImage.getBytes(), 0, croppedImage.getBytes().length);
			fios.close();
		} catch (IOException e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorcam"));
		}
	}

	public void preparedlgphoto() {
		filename = "";
	}

	public void crop() {
		if (croppedImage == null) {
			return;
		}

		cropperimagesave(filepath());
//		cropperimagesave("C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\images\\" + filename + ".png");

		//filename = filename.concat("_cropper");
	}

	public void loadingphotoupload(FileUploadEvent event) {
		data = event.getFile().getContents();
		filename = getRandomImageName();
		
//		imagesave("C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\images\\" + filename + ".png");
//
//		if (ImageUtil.verifyImageResolution("C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\images\\" + filename + ".png")) {
//			ImageUtil.convertImageAndDecrease("C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\images\\" + filename + ".png");
//		}
		
		imagesave(filepath());

		if (ImageUtil.verifyImageResolution(filepath())) {
			ImageUtil.convertImageAndDecrease(filepath());
		}
	}
}
