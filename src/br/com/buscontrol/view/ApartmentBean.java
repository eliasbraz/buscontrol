package br.com.buscontrol.view;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;
import org.primefaces.event.SelectEvent;

import br.com.buscontrol.control.UserControl;
import br.com.buscontrol.dao.ApartmentDAO;
import br.com.buscontrol.dao.BuildingDAO;
import br.com.buscontrol.model.Apartment;
import br.com.buscontrol.model.Building;
import br.com.buscontrol.util.FacesUtil;

@Named(value = "abean")
@ViewScoped
public class ApartmentBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private ApartmentDAO daoapartment;

	@Inject
	private BuildingDAO daobuilding;

	@Inject
	private ConfPreferences preferences;

	@Inject
	private UserControl usercontrol;

	private Apartment apartment;
	private Apartment seletedapartment;
	private ArrayList<Apartment> apartments = null;
	private ArrayList<Building> buildings = null;

	@PostConstruct
	public void init() {
		apartment = new Apartment();
		apartments = daoapartment.findall();
		buildings = daobuilding.listavaiables();
		preferences.setDisabled(false);
		preferences.setWindowtype("new");
		preferences.setTitle("Apartments");
	}

	public Apartment getApartment() {
		return apartment;
	}

	public void setApartment(Apartment apartment) {
		this.apartment = apartment;
	}

	public Apartment getSeletedapartment() {
		return seletedapartment;
	}

	public void setSeletedapartment(Apartment seletedapartment) {
		this.seletedapartment = seletedapartment;
	}

	public ArrayList<Apartment> getApartments() {
		return apartments;
	}

	public void setApartments(ArrayList<Apartment> apartments) {
		this.apartments = apartments;
	}

	public ArrayList<Building> getBuildings() {
		return buildings;
	}

	public void setBuildings(ArrayList<Building> buildings) {
		this.buildings = buildings;
	}

	public void listall() {
		try {
			apartments = daoapartment.findall();
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorlisting"));
		}
	}

	public void saveorupdate() {
		try {
			apartment.setUser(usercontrol.getUser());
			daoapartment.saveorupdate(apartment);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("savesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorsaving"));
		}
	}

	public void preparebean(SelectEvent event) {
		setApartment(getSeletedapartment());
		preferences.setDisabled(true);
		preferences.setWindowtype("open");
	}

	public void disable(ActionEvent event) {
		try {
			apartment = (Apartment) event.getComponent().getAttributes().get("selectedapartment");

			daoapartment.delete(apartment);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("disablesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errordisable"));
		}
	}
}
