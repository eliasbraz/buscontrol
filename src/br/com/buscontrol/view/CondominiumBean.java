package br.com.buscontrol.view;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;
import org.primefaces.event.SelectEvent;

import br.com.buscontrol.control.UserControl;
import br.com.buscontrol.dao.CondominiumDAO;
import br.com.buscontrol.model.Condominium;
import br.com.buscontrol.util.FacesUtil;

@Named(value = "cdbean")
@ViewScoped
public class CondominiumBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private CondominiumDAO daocondominium;

	@Inject
	private ConfPreferences preferences;

	@Inject
	private UserControl usercontrol;

	private Condominium condominium;
	private Condominium selectedcondominium;
	private ArrayList<Condominium> condominiums = null;

	@PostConstruct
	public void init() {
		condominium = new Condominium();
		condominiums = daocondominium.findall();
		preferences.setDisabled(false);
		preferences.setWindowtype("new");
		preferences.setTitle("Condominiums");
	}

	public Condominium getCondominium() {
		return condominium;
	}

	public void setCondominium(Condominium condominium) {
		this.condominium = condominium;
	}

	public Condominium getSelectedcondominium() {
		return selectedcondominium;
	}

	public void setSelectedcondominium(Condominium selectedcondominium) {
		this.selectedcondominium = selectedcondominium;
	}

	public ArrayList<Condominium> getCondominiums() {
		return condominiums;
	}

	public void setCondominiums(ArrayList<Condominium> condominiums) {
		this.condominiums = condominiums;
	}

	public void listall() {
		try {
			condominiums = daocondominium.findall();
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorlisting"));
		}
	}

	public void saveorupdate() {
		try {
			condominium.setUser(usercontrol.getUser());
			daocondominium.saveorupdate(condominium);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("savesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errorsaving"));
		}
	}

	public void preparebean(SelectEvent event) {
		setCondominium(getSelectedcondominium());
		preferences.setDisabled(true);
		preferences.setWindowtype("open");
	}

	public void disable(ActionEvent event) {
		try {
			condominium = (Condominium) event.getComponent().getAttributes().get("selectedcondominium");

			daocondominium.delete(condominium);

			init();

			Messages.addGlobalInfo(FacesUtil.getLabel("disablesuccess"));
		} catch (Exception e) {
			e.printStackTrace();

			Messages.addGlobalError(FacesUtil.getLabel("errordisable"));
		}
	}
}
