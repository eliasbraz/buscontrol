package br.com.buscontrol.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.transaction.Transactional;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

/**
 * Entity implementation class for Entity: ResidentTrip
 *
 */
@Entity

public class ResidentTrip implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Trip trip;
	private Resident resident;
	private Date time;

	private Date initialdate;
	private Date finaldate;

	public ResidentTrip() {
		super();
		trip = new Trip();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "trip_id", nullable = false)
	public Trip getTrip() {
		return this.trip;
	}

	public void setTrip(Trip trip) {
		this.trip = trip;
	}

	@OneToOne()
	@JoinColumn(nullable = false)
	public Resident getResident() {
		return this.resident;
	}

	public void setResident(Resident resident) {
		this.resident = resident;
	}

	@JsonFormat(shape = Shape.STRING, pattern = "HH:mm:ss")
	@Temporal(TemporalType.TIME)
	@Column(nullable = false)
	public Date getTime() {
		return this.time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@Transactional
	public Date getInitialdate() {
		return initialdate;
	}

	public void setInitialdate(Date initialdate) {
		this.initialdate = initialdate;
	}

	@Transactional
	public Date getFinaldate() {
		return finaldate;
	}

	public void setFinaldate(Date finaldate) {
		this.finaldate = finaldate;
	}

}
