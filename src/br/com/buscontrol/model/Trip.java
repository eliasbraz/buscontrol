package br.com.buscontrol.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

/**
 * Entity implementation class for Entity: Trip
 *
 */
@XmlRootElement
@Entity
public class Trip implements Serializable {

	private Integer id;
	private Route route;
	private User user;
	private Date date;
	private Date time;
	private List<ResidentTrip> residents;
	private static final long serialVersionUID = 1L;

	public Trip() {
		super();
		residents = new ArrayList<>();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@OneToOne
	@JoinColumn(nullable = false)
	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	@OneToOne
	@JoinColumn(nullable = false)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@JsonFormat(shape = Shape.STRING, pattern = "HH:mm:ss")
	@Temporal(TemporalType.TIME)
	@Column(nullable = false)
	public Date getTime() {
		return this.time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "trip", cascade = CascadeType.ALL, targetEntity = ResidentTrip.class)
	public List<ResidentTrip> getResidents() {
		for (ResidentTrip residentTrip : residents) {
			residentTrip.setTrip(this);
		}
		return residents;
	}

	public void setResidents(List<ResidentTrip> residents) {
		this.residents = residents;
	}

}
