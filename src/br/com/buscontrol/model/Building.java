package br.com.buscontrol.model;

import br.com.buscontrol.model.Condominium;
import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.UpdateTimestamp;

/**
 * Entity implementation class for Entity: Building
 *
 */
@Entity
@NamedQueries(@NamedQuery(name = "building.avaiables", query = "select b from Building b where b.active = true"))
public class Building implements Serializable {

	private Integer id;
	private String name;
	private Condominium condominium;
	private boolean active;
	private User user;
	private Date registerdate;
	private static final long serialVersionUID = 1L;

	public Building() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false, length = 30)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToOne
	@JoinColumn(nullable = false)
	public Condominium getCondominium() {
		return this.condominium;
	}

	public void setCondominium(Condominium condominium) {
		this.condominium = condominium;
	}

	public boolean getActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@OneToOne
	@JoinColumn(nullable = false)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	public Date getRegisterdate() {
		return registerdate;
	}

	public void setRegisterdate(Date registerdate) {
		this.registerdate = registerdate;
	}
}
