package br.com.buscontrol.model;

public enum UserType {
	ADMINISTRATOR("Administrator"), OPERATOR("Operator"), DRIVER("Driver");

	private String type;

	private UserType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

}
