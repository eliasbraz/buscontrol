package br.com.buscontrol.model;

import br.com.buscontrol.model.VehicleType;
import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.UpdateTimestamp;

/**
 * Entity implementation class for Entity: Vehicle
 *
 */
@Entity
@NamedQueries(@NamedQuery(name = "vehicle.avaiables", query = "select v from Vehicle v where v.active = true"))
public class Vehicle implements Serializable {

	private Integer id;
	private VehicleType type;
	private String boardnumber;
	private boolean active;
	private User user;
	private Date registerdate;
	private static final long serialVersionUID = 1L;

	public Vehicle() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@OneToOne
	@JoinColumn(nullable = false)
	public VehicleType getType() {
		return this.type;
	}

	public void setType(VehicleType type) {
		this.type = type;
	}

	@Column(nullable = false, length = 8, unique = true)
	public String getBoardnumber() {
		return this.boardnumber;
	}

	public void setBoardnumber(String boardnumber) {
		this.boardnumber = boardnumber;
	}

	public boolean getActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@OneToOne
	@JoinColumn(nullable = false)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	public Date getRegisterdate() {
		return registerdate;
	}

	public void setRegisterdate(Date registerdate) {
		this.registerdate = registerdate;
	}

	@Override
	public String toString() {
		return "Vehicle [id=" + id + ", type=" + type + ", boardnumber=" + boardnumber + ", active=" + active + "]";
	}

}
