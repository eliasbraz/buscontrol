package br.com.buscontrol.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class UserTypeConverter implements AttributeConverter<UserType, String> {

	@Override
	public String convertToDatabaseColumn(UserType type) {
		switch (type) {
		case ADMINISTRATOR:
			return "ADM";
		case OPERATOR:
			return "OPE";
		case DRIVER:
			return "DRI";
		}
		return null;
	}

	@Override
	public UserType convertToEntityAttribute(String type) {
		switch (type) {
		case "ADM":
			return UserType.ADMINISTRATOR;
		case "OPE":
			return UserType.OPERATOR;
		case "DRI":
			return UserType.DRIVER;
		}
		return null;
	}

}
