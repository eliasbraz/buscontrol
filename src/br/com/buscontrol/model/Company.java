package br.com.buscontrol.model;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.UpdateTimestamp;

/**
 * Entity implementation class for Entity: Company
 *
 */
@Entity
@NamedQueries(@NamedQuery(name = "company.avaiables", query = "select c from Company c where c.active = true"))
public class Company implements Serializable {

	private Integer id;
	private String cnpj;
	private String fantasyname;
	private String socialname;
	private String address;
	private String city;
	private String phone;
	private String cellphone;
	private boolean active;
	private User user;
	private Date registerdate;
	private static final long serialVersionUID = 1L;

	public Company() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false, length = 18, unique = true)
	public String getCnpj() {
		return this.cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	@Column(nullable = false, length = 30)
	public String getFantasyname() {
		return this.fantasyname;
	}

	public void setFantasyname(String fantasyname) {
		this.fantasyname = fantasyname;
	}

	@Column(nullable = false, length = 50)
	public String getSocialname() {
		return this.socialname;
	}

	public void setSocialname(String socialname) {
		this.socialname = socialname;
	}

	@Column(nullable = false, length = 100)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(nullable = false, length = 30)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(length = 14)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(length = 14)
	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public boolean getActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@OneToOne
	@JoinColumn(nullable = false)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	public Date getRegisterdate() {
		return registerdate;
	}

	public void setRegisterdate(Date registerdate) {
		this.registerdate = registerdate;
	}
}
