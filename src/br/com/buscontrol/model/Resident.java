package br.com.buscontrol.model;

import br.com.buscontrol.model.Apartment;
import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.UpdateTimestamp;

/**
 * Entity implementation class for Entity: Resident
 *
 */
@Entity
@NamedQueries(@NamedQuery(name = "resident.avaiables", query = "select r from Resident r where r.active = true"))
public class Resident implements Serializable {

	private Integer id;
	private String document;
	private String name;
	private Apartment apartment;
	private boolean active;
	private User user;
	private Date registerdate;
			
	private static final long serialVersionUID = 1L;

	public Resident() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length = 30, unique = true)
	public String getDocument() {
		return this.document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	@Column(length = 30)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToOne(cascade = CascadeType.ALL)
	public Apartment getApartment() {
		return this.apartment;
	}

	public void setApartment(Apartment apartment) {
		this.apartment = apartment;
	}

	public boolean getActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@OneToOne
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	public Date getRegisterdate() {
		return registerdate;
	}

	public void setRegisterdate(Date registerdate) {
		this.registerdate = registerdate;
	}
}
