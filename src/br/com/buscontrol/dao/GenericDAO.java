package br.com.buscontrol.dao;

import java.io.Serializable;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public abstract class GenericDAO<Entity> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	protected EntityManager em;

	private Class<Entity> entity;

	public GenericDAO(Class<Entity> entity) {
		this.entity = entity;
	}

	public Entity saveorupdate(Entity entity) {
		try {
			em.getTransaction().begin();
			entity = em.merge(entity);
			em.getTransaction().commit();

			return entity;
		} catch (Exception e) {
			transactionrollback(e);
			throw e;
		}
	}

	public void delete(Entity entity) {
		try {
			em.getTransaction().begin();
			em.remove(entity);
			em.getTransaction().commit();
		} catch (Exception e) {
			transactionrollback(e);
			throw e;
		}
	}

	public void deletebyid(int id) {
		try {
			Entity entity = findbyid(id);
			delete(entity);
		} catch (Exception e) {
			transactionrollback(e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Entity> findall() {
		try {
			return (ArrayList<Entity>) em.createQuery("select e from " + entity.getSimpleName() + " e").getResultList();
		} catch (Exception e) {
			transactionrollback(e);
			throw e;
		}
	}

	public Entity findbyid(int id) {
		try {
			return (Entity) em.find(entity, id);
		} catch (Exception e) {
			transactionrollback(e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public Entity findbyname(String name) {
		try {
			return (Entity) em.createNamedQuery("vehicletype.findname").setParameter("value1", name).getResultList();
		} catch (Exception e) {
			transactionrollback(e);
			throw e;
		}
	}

	protected void transactionrollback(Exception e) {
		if (em.getTransaction() != null) {
			em.getTransaction().rollback();
		}
	}
}
