package br.com.buscontrol.dao;

import java.util.ArrayList;

import br.com.buscontrol.model.Resident;

public class ResidentDAO extends GenericDAO<Resident> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ResidentDAO() {
		super(Resident.class);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Resident> listavaiables() {
		try {
			return (ArrayList<Resident>) em.createNamedQuery("resident.avaiables").getResultList();
		} catch (Exception e) {
			transactionrollback(e);
			throw e;
		}
	}
}
