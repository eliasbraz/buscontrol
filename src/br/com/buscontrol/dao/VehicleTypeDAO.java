package br.com.buscontrol.dao;

import br.com.buscontrol.model.VehicleType;

public class VehicleTypeDAO extends GenericDAO<VehicleType> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VehicleTypeDAO() {
		super(VehicleType.class);
	}
}
