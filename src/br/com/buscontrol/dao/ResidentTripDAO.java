package br.com.buscontrol.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.buscontrol.model.ResidentTrip;

public class ResidentTripDAO extends GenericDAO<ResidentTrip> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ResidentTripDAO() {
		super(ResidentTrip.class);
	}

	public ArrayList<ResidentTrip> residenttripfilterred(ResidentTrip residentTrip) throws ParseException {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<ResidentTrip> criteria = builder.createQuery(ResidentTrip.class);
		Root<ResidentTrip> root = criteria.from(ResidentTrip.class);

		if (residentTrip.getInitialdate() != null) {
			String initialdate = new SimpleDateFormat("yyyy-MM-dd").format(residentTrip.getInitialdate());
			criteria.where(builder.greaterThanOrEqualTo(root.get("trip").get("date"),
					new SimpleDateFormat("yyyy-MM-dd").parse(initialdate)));
		}

		if (residentTrip.getFinaldate() != null) {
			String finaldate = new SimpleDateFormat("yyyy-MM-dd").format(residentTrip.getFinaldate());
			criteria.where(builder.lessThanOrEqualTo(root.get("trip").get("date"),
					new SimpleDateFormat("yyyy-MM-dd").parse(finaldate)));
		}

		if (residentTrip.getTrip().getUser() != null) {
			criteria.where(builder.equal(root.get("trip").get("user"), residentTrip.getTrip().getUser()));
		}

		if (residentTrip.getTrip().getRoute() != null) {
			criteria.where(builder.equal(root.get("trip").get("route"), residentTrip.getTrip().getRoute()));
		}

		if (residentTrip.getResident() != null) {
			criteria.where(builder.equal(root.get("resident"), residentTrip.getResident()));
		}

		return (ArrayList<ResidentTrip>) em.createQuery(criteria).getResultList();
	}
}
