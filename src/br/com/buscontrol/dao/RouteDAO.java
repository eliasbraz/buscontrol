package br.com.buscontrol.dao;

import java.util.ArrayList;

import br.com.buscontrol.model.Route;

public class RouteDAO extends GenericDAO<Route> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RouteDAO() {
		super(Route.class);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Route> listavaiables() {
		try {
			return (ArrayList<Route>) em.createNamedQuery("route.avaiables").getResultList();
		} catch (Exception e) {
			transactionrollback(e);
			throw e;
		}
	}
}
