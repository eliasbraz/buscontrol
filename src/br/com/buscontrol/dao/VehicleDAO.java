package br.com.buscontrol.dao;

import java.util.ArrayList;

import br.com.buscontrol.model.Vehicle;

public class VehicleDAO extends GenericDAO<Vehicle> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VehicleDAO() {
		super(Vehicle.class);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Vehicle> listavaiables() {
		try {
			return (ArrayList<Vehicle>) em.createNamedQuery("vehicle.avaiables").getResultList();
		} catch (Exception e) {
			transactionrollback(e);
			throw e;
		}
	}
}
