package br.com.buscontrol.dao;

import java.util.ArrayList;

import br.com.buscontrol.model.Company;

public class CompanyDAO extends GenericDAO<Company> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CompanyDAO() {
		super(Company.class);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Company> listavaiables() {
		try {
			return (ArrayList<Company>) em.createNamedQuery("company.avaiables").getResultList();
		} catch (Exception e) {
			if (em.getTransaction() != null) {
				em.getTransaction().rollback();
				throw e;
			}
			return null;
		}
	}
}
