package br.com.buscontrol.dao;

import java.util.ArrayList;

import br.com.buscontrol.model.Apartment;

public class ApartmentDAO extends GenericDAO<Apartment> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ApartmentDAO() {
		super(Apartment.class);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Apartment> listavaiables() {
		try {
			return (ArrayList<Apartment>) em.createNamedQuery("apartment.avaiables").getResultList();
		} catch (Exception e) {
			transactionrollback(e);
			throw e;
		}
	}
}
