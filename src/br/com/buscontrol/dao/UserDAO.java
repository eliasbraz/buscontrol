package br.com.buscontrol.dao;

import java.util.ArrayList;

import javax.inject.Named;

import br.com.buscontrol.model.User;

@Named
public class UserDAO extends GenericDAO<User> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserDAO() {
		super(User.class);
	}

	public User findbyusername(String username) {
		try {
			return (User) em.createNamedQuery("user.findbyusername").setParameter("username", username).getSingleResult();
		} catch (Exception e) {
			if (em.getTransaction() != null) {
				em.getTransaction().rollback();
				throw e;
			}
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public ArrayList<User> listavaiables() {
		try {
			return (ArrayList<User>) em.createNamedQuery("user.avaiables").getResultList();
		} catch (Exception e) {
			transactionrollback(e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<User> listdrivers() {
		try {
			return (ArrayList<User>) em.createNamedQuery("user.driversonly").getResultList();
		} catch (Exception e) {
			transactionrollback(e);
			throw e;
		}
	}
}
