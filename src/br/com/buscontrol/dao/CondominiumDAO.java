package br.com.buscontrol.dao;

import java.util.ArrayList;

import br.com.buscontrol.model.Condominium;

public class CondominiumDAO extends GenericDAO<Condominium> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CondominiumDAO() {
		super(Condominium.class);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Condominium> listavaiables() {
		try {
			return (ArrayList<Condominium>) em.createNamedQuery("condominium.avaiables").getResultList();
		} catch (Exception e) {
			transactionrollback(e);
			throw e;
		}
	}
}
