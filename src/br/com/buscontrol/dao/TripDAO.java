package br.com.buscontrol.dao;

import br.com.buscontrol.model.Trip;

public class TripDAO extends GenericDAO<Trip> {

	private static final long serialVersionUID = 1L;

	public TripDAO() {
		super(Trip.class);
	}
}
