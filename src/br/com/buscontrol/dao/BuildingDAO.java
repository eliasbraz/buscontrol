package br.com.buscontrol.dao;

import java.util.ArrayList;

import br.com.buscontrol.model.Building;

public class BuildingDAO extends GenericDAO<Building> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BuildingDAO() {
		super(Building.class);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Building> listavaiables() {
		try {
			return (ArrayList<Building>) em.createNamedQuery("building.avaiables").getResultList();
		} catch (Exception e) {
			transactionrollback(e);
			throw e;
		}
	}
}
