package br.com.buscontrol.filter;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import br.com.buscontrol.control.UserControl;

@WebFilter(urlPatterns = { "/pages/*", "/templates/*" }, servletNames = "{Login}")
public class LoginFilter extends AbstractFilter implements Filter {

	@Inject
	private UserControl usercontrol;

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		if (!usercontrol.isLogged()) {
			doLogin(request, response, (HttpServletRequest) request);
		} else {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

}
