package br.com.buscontrol.filter;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AbstractFilter {

	protected void doLogin(ServletRequest request, ServletResponse response, HttpServletRequest req) throws ServletException, IOException {
		((HttpServletResponse) response).sendRedirect("../login.xhtml");
		//req.getRequestDispatcher("../login.xhtml").forward(request, response);
	}
	
	protected void accessDenied(ServletRequest request, ServletResponse response, HttpServletRequest req) throws ServletException, IOException {
		req.getRequestDispatcher("accessdenied").forward(request, response);
	}
}
